from redis_cache import RedisCache

from core.models import Url


class UrlRedisCache(RedisCache):
    """
    Custom Redis Cache tailored for this project
    """

    def get_many(self, keys, version=None):
        # override default implementation to update cache if session is still alive
        d = {}
        for k in keys:
            val = self.get(k, self._missing_key, version=version)
            if val is not self._missing_key:
                d[k] = val
            else:
                try:
                    # get Url object and set cache (full, (hashid, short, clicks))
                    u = Url.objects.get(full=k)
                    hashid, short, clicks = u.pk, u.short, u.clicks
                    self.set(k, (hashid, short, clicks))
                    # also update response dict
                    d[k] = (hashid, short, clicks)
                except Url.DoesNotExist:
                    pass
        return d
