from cut.settings import *  # noqa

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.db.DatabaseCache",
        "LOCATION": "cache",
        "OPTIONS": {"MAX_ENTRIES": 1000},
    },
    "url": {
        "BACKEND": "cut.redis.UrlRedisCache",
        "TIMEOUT": 0.5,
        "LOCATION": os.environ.get("REDIS_CACHE", "localhost:6379"),  # noqa
    },
}
