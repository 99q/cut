import pytest
from django.core.cache import caches

from tests.factories.url_factory import UrlFactory


@pytest.fixture
def url():
    return UrlFactory.create()


@pytest.fixture
def cache():
    return caches["url"]
