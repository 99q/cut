import pytest
from django.urls import reverse

from core.models import Url


urls = [
    "https://google.com",
    "https://ya.ru",
    "https://youtube.com",
    "http://github.com",
]
fake_urls = ["google", "ya", "youtube", "github"]

pytestmark = pytest.mark.django_db


@pytest.mark.parametrize("full", urls)
def test_create_short_success(client, full):
    r = client.post(reverse("core:index"), {"full": full})
    assert Url.objects.last().full == full
    assert client.session.get("full") == {full}
    r = client.get(reverse("core:index"))
    assert r.context_data["urls"]


@pytest.mark.parametrize("full", fake_urls)
def test_create_short_fail(client, full):
    r = client.post(reverse("core:index"), {"full": full})
    assert not Url.objects.exists()
    assert not client.session.get("full")
    r = client.get(reverse("core:index"))
    assert "urls" not in r.context_data


def test_create_multiple_shorts(client):
    urls = {
        "https://google.com",
        "https://ya.ru",
        "https://youtube.com",
        "http://github.com",
    }
    [client.post(reverse("core:index"), {"full": u}) for u in urls]
    assert client.session.get("full") == urls


def test_redirect_short(client, url):
    r = client.get(reverse("core:redirect", kwargs={"short": url.short}))
    assert r.status_code == 302
    assert r.url == url.full
    url.refresh_from_db()
    assert url.clicks == 1


def test_delete_full(client, url):
    client.delete(
        reverse("core:delete", kwargs={"pk": url.id_hashid.id}),
    )
    with pytest.raises(Url.DoesNotExist):
        url.refresh_from_db()


def test_get_delete_page(client, url):
    client.get(
        reverse("core:delete", kwargs={"pk": url.id_hashid.id}),
    )
    with pytest.raises(Url.DoesNotExist):
        url.refresh_from_db()


def test_redis_cache(cache, url):
    assert url.full in cache.get_many([url.full])
    url.delete()
    assert not cache.get_many([url.full])
