import factory
from factory.django import DjangoModelFactory

from core.models import Url


class UrlFactory(DjangoModelFactory):
    full = factory.Faker("url")

    class Meta:
        model = Url
