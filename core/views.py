import short_url
from django import views
from django.core.cache import caches
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy

from core.models import Url


class IndexView(views.generic.CreateView):
    """
    View to create short urls
    and list all generated links belonging to a particular session
    """

    model = Url
    fields = ("full",)
    success_url = reverse_lazy("core:index")
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # get full links of created Url objects by a spec. session
        full = self.request.session.get("full")
        # if they exist, add dict like {full: (hashid, short, clicks)} to context
        if full:
            context["urls"] = dict(
                sorted(
                    caches["url"].get_many(full).items(),
                    key=lambda item: item[1][2],
                    reverse=True,
                )
            )
        return context

    def form_valid(self, form):
        # override form_valid to get created object
        self.object = form.save()
        # if session does not exist, create it and add object.full to session data
        if not self.request.session.session_key:
            self.request.session["full"] = {self.object.full}
            self.request.session.save()
        # if session already exists, extend list of ids by adding new Url object
        else:
            self.request.session["full"].add(self.object.full)
            # indicate that session object has been modified
            self.request.session.modified = True
        return super().form_valid(form)


class UrlRedirectView(views.generic.RedirectView):
    """
    View to redirect from short url to full
    """

    def get_redirect_url(self, *args, **kwargs):
        # get object by short url
        obj = get_object_or_404(Url, pk=short_url.decode_url(kwargs["short"]))
        # increase the number of views
        obj.click()
        # return full link
        return obj.full


class UrlDeleteView(views.generic.DeleteView):
    """
    View to delete Url object by hashid
    """

    model = Url
    success_url = reverse_lazy("core:index")
    pk_url_kwarg = "pk"

    def get(self, request, *args, **kwargs):
        # To avoid to render template with confirmation, just disable get method
        return self.post(request, *args, **kwargs)
