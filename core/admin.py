from django.contrib import admin

from core.models import Url


@admin.register(Url)
class UrlAdmin(admin.ModelAdmin):
    """
    Default Url ModelAdmin
    """

    # because of auto_now_add(editable=False) of datetime field
    readonly_fields = ("created",)
