import short_url
from django.core.cache import caches
from django.db import models
from hashid_field import BigHashidAutoField


class Url(models.Model):
    """
    Url model
    Attributes:
        id - primary key
        full - origin link
        clicks - number of views
        created - datetime of creation
    """

    id = BigHashidAutoField(primary_key=True)
    full = models.URLField(unique=True)
    clicks = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        # default ordering by clicks
        ordering = ["-clicks"]

    @property
    def short(self):
        # generate short link by url primary key
        return short_url.encode_url(self.id_hashid.id)

    def click(self):
        # increase number of views and save object
        self.clicks += 1
        self.save()

    def _cache_urls(self, created=False):
        # set cache (full, (hashid, short, clicks))
        if created:
            caches["url"].set(self.full, (self.pk, self.short, self.clicks))

    def save(self, *args, **kwargs):
        created = self._state.adding is True
        super().save(*args, **kwargs)
        self._cache_urls(created)

    def __str__(self):
        return self.full
