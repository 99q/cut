from django.urls import path

from core import views

app_name = "core"

urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("delete/<str:pk>", views.UrlDeleteView.as_view(), name="delete"),
    path("<str:short>/", views.UrlRedirectView.as_view(), name="redirect"),
]
